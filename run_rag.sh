#!/bin/bash

# SPDX-FileCopyrightText: 2024 piistis
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

#SBATCH --job-name=TravailGPU # name of job
#SBATCH --output=run/TravailGPU%j.out # output file (%j = job ID)
#SBATCH --error=run/TravailGPU%j.err # error file (%j = job ID)
#SBATCH --constraint=a100 # reserve 80 GB A100 GPUs
#SBATCH --nodes=1 # reserve 2 nodez
#SBATCH --ntasks=1 # reserve 16 tasks (or processes)
#SBATCH --gres=gpu:2 # reserve 8 GPUs per node
#SBATCH --cpus-per-task=20 # reserve 8 CPUs per task (and associated memory)
#SBATCH --time=00:30:00 # maximum allocation time "(HH:MM:SS)"
#SBATCH --hint=nomultithread # deactivate hyperthreading
#SBATCH --account=olh@a100 # A100 accounting

module purge # purge modules inherited by default

module load cpuarch/amd # select modules compiled for AMD
module load pytorch-gpu/py3/2.1.1 # load modules

set -x 

srun python -u mistral_rag.py
