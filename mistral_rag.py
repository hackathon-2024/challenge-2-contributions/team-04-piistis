# SPDX-FileCopyrightText: 2024 piistis
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import torch
import pandas as pd
from transformers import AutoTokenizer, AutoModelForCausalLM, BitsAndBytesConfig, pipeline
import pathlib
import time
import elasticsearch
import os
import re
import sys

# Run git clone https://code.peren.fr/hackathon-2024/retrieval-modules/platform-docs-versions.git
# before the first run of this code

q = sys.argv[1]

questions = pd.read_csv(q, sep=';')

repo = pathlib.Path("platform-docs-versions")

question_srcs = []

results = pathlib.Path("/results")

for i in range(len(questions)):
    segments = []
    source_files = open(f'./results/{i}_source.txt', 'r', encoding='utf-8').readlines()

    for j in range(min(5, len(source_files))):
        f = open(f'./results/{i}_segment_{j}.txt', 'r', encoding='utf-8')
        chunk = f.read()

        segments.append({'source': source_files[j], 'text': chunk})
    question_srcs.append(segments)

print(len(question_srcs))
# Extract chunks of data from the markdown files

#for path in repo.rglob("*/*.md"):
#    f = open(path, "r", encoding="utf-8")
#    split = f.read().split()
#    n = 200 # Set the chunk length (in words)
#    for i in range(0, len(split), n):
#        texts.append(' '.join(split[i:i+n]))
#        sources.append(str(path))

#df = pd.DataFrame.from_dict({'text': texts, 'source': sources})
# df.to_csv('./data/preprocessed.csv')

data = []

# Take 20 random chunks

#rand_docs = df.sample(20)

#for i in range(len(rand_docs)):
#    row = rand_docs.iloc[i]
#    data.append({'source': row.source, 'text': row.text})

#print(len(data))

bnb_config = BitsAndBytesConfig(
    load_in_4bit=True,
    bnb_4bit_quant_type="nf4",
    bnb_4bit_use_double_quant=True,
)

model = AutoModelForCausalLM.from_pretrained('mistralai/Mistral-7B-Instruct-v0.2',
                                            device_map="auto",
                                            load_in_4bit=True,
                                            quantization_config=bnb_config,
                                            torch_dtype=torch.bfloat16)

tokenizer = AutoTokenizer.from_pretrained('mistralai/Mistral-7B-Instruct-v0.2')

curr_time = str(time.time()).split('.')[0]

out_file = f"./output{curr_time}/"
os.mkdir(out_file)

answer_file = f"./output{curr_time}/answers/"
os.mkdir(answer_file)

source_file = f"./output{curr_time}/sources/"
os.mkdir(source_file)

prompt_file = f"./output{curr_time}/prompts/"
os.mkdir(prompt_file)

# [INST] and [/INST] are tokens used in mistral training for marking the start and end of an instruction
for i in range(len(questions)):
    question = questions.iloc[i]
    q_id = question.id
    q_text = question.question

    data = question_srcs[i]

    prepared_text = f"""
    [INST] [System]: You are a friendly chatbot assistant that responds in a conversational
    manner to user's questions. Respond in short but complete answers. Use Context to inform your answers.
    Cite the exact source url your information comes from. Create only a single answer.
	If the question is in another language than English, make sure the answer is in the question's language.
    If none of the texts provided in the context contain the answer to the question, do not answer and state that you do not have enough information.
    ---
    [Context]: {data}
    --- [/INST]
    [User]: {q_text}
    ---
    [Answer]:
    """

    tokenized_text = tokenizer(prepared_text,
                                  return_tensors="pt").input_ids.to('cuda')

    summary_ids = model.generate(tokenized_text, min_new_tokens=100, max_new_tokens=512)

    out = tokenizer.batch_decode(summary_ids,
                                 skip_special_tokens=True)[-1]

# print(out)

    out_split = out.split('[Answer]:')
    ans = out_split[-1]
    question = out_split[0].split('[User]:')[-1]

    in_srcs = [x['source'] for x in data]

    reg = r"https?:\/\/[www\.]?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b[-a-zA-Z0-9@:%_\+.~#?&//=]*"
    found_sources = re.findall(reg, ans)

    src_str = ""
    for source in found_sources:
        if source in ans:
            src_str += f"{source}\n"

    print(f'Question: {question}')
    print(f'Answer: {ans}')

    with open(file=f'{answer_file}/{q_id:03d}.txt', 
              mode='w',
              encoding="utf-8") as file:
        file.write(ans)
    with open(file=f'{source_file}/{q_id:03d}.txt', 
              mode='w',
              encoding="utf-8") as file:
        file.write(str(src_str))
    with open(file=f'{prompt_file}/{q_id:03d}.txt', 
              mode='w',
              encoding="utf-8") as file:
        file.write(prepared_text)
