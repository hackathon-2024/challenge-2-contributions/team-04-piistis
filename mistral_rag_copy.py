# SPDX-FileCopyrightText: 2024 piistis
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import torch
import pandas as pd
from transformers import AutoTokenizer, AutoModelForCausalLM, BitsAndBytesConfig, pipeline
import pathlib
import time
import elasticsearch
import os
import re

# Run git clone https://code.peren.fr/hackathon-2024/retrieval-modules/platform-docs-versions.git
# before the first run of this code

repo = pathlib.Path("platform-docs-versions/Linkedin_Community-Report")

texts = []
sources = []

# Extract chunks of data from the markdown files

for path in repo.rglob("*/*.md"):
    f = open(path, "r", encoding="utf-8")
    split = f.read().split()
    n = 200 # Set the chunk length (in words)
    for i in range(0, len(split), n):
        texts.append(' '.join(split[i:i+n]))
        sources.append(str(path))

df = pd.DataFrame.from_dict({'text': texts, 'source': sources})
df.to_csv('./data/preprocessed.csv')

data = []

# Take 20 random chunks

rand_docs = df

for i in range(len(df)):
    row = rand_docs.iloc[i]
    data.append({'source': row.source, 'text': row.text})

print(len(data))

bnb_config = BitsAndBytesConfig(
    load_in_4bit=True,
    bnb_4bit_quant_type="nf4",
    bnb_4bit_use_double_quant=True,
)

model = AutoModelForCausalLM.from_pretrained('mistralai/Mistral-7B-Instruct-v0.2',
                                            device_map="auto",
                                            load_in_4bit=True,
                                            quantization_config=bnb_config,
                                            torch_dtype=torch.bfloat16)
tokenizer = AutoTokenizer.from_pretrained('mistralai/Mistral-7B-Instruct-v0.2')

questions = pd.read_csv("./challenge-2-dataset-and-documentation/dataset/train/input/questions.csv", sep=';')

curr_time = str(time.time()).split('.')[0]

out_file = f"./output{curr_time}/"
os.mkdir(out_file)

answer_file = f"./output{curr_time}/answers/"
os.mkdir(answer_file)

source_file = f"./output{curr_time}/sources/"
os.mkdir(source_file)

# [INST] and [/INST] are tokens used in mistral training for marking the start and end of an instruction
for i in range(1):
    question = questions.iloc[i]
    q_id = question.id
    data = """{'source': https://values.snap.com/privacy/transparency,
'data': 'Transparency Report
January 1, 2023 – June 30, 2023
Released:
October 25, 2023
Updated:
December 13, 2023


To provide insight into Snap’s safety efforts and the nature and volume of content reported on our platform, we publish transparency reports twice a year. We are committed to continuing to make these reports more comprehensive and informative to the many stakeholders who care deeply about our content moderation and law enforcement practices, as well as the well-being of our community. 
This Transparency Report covers the first half of 2023 (January 1 - June 30). As with our previous reports, we share data about the global number of in-app content and account-level reports we received and enforced against across specific categories of policy violations; how we responded to requests from law enforcement and governments; and our enforcement actions broken down by country.

As part of our ongoing commitment to continually improve our transparency reports, we are introducing a few new elements with this release. We have added additional data points around our advertising practices and moderation, as well as content and account appeals. In alignment with the EU Digital Services Act, we’ve also added new contextual information around our operations in EU Member States, such as the number of content moderators and monthly active users (MAUs) in the region. Much of this information can be found throughout the report, and in our Transparency Center’s dedicated European Union page.
Finally, we have updated our Glossary with links to our Community Guidelines explainers, which provide additional context around our platform policy and operational efforts. 
For more information about our policies for combating online harms, and plans to continue evolving our reporting practices, please read our recent Safety & Impact blog about this Transparency Report. 
To find additional resources for safety and privacy on Snapchat, see our About Transparency Reporting tab at the bottom of the page.
Please note that the most up-to-date version of this Transparency Report can be found in the en-US locale.
Overview of Content and Account Violations

From January 1 - June 30, 2023, Snap enforced against 6,216,118 pieces of content globally that violated our policies.
During the reporting period, we saw a Violative View Rate (VVR) of 0.02 percent, which means that out of every 10,000 Snap and Story views on Snapchat, 2 contained content found to violate our policies.
Total Content & Account Reports Total Content Enforced  Total Unique Accounts Enforced
17,267,985  6,216,118   3,687,082
Reason  Content & Account Reports   Content Enforced    % of Total Content Enforced Unique Accounts Enforced    Turnaround Time (in median minutes)
Sexual Content  5,843,879   3,270,318   52.6%   1,833,194   6
Harassment and Bullying 6,645,969   920,070 14.8%   778,417 13
Threats & Violence  615,011 94,556  1.5%    73,981  21
Self-Harm & Suicide 122,369 24,621  0.4%    22,637  21
False Information   387,242 238 0.0%    209 12
Impersonation   521,168 12,379  0.2%    12,314  2
Spam    1,924,768   1,380,341   22.2%   941,653 <1
Drugs   642,421 297,554 4.8%    203,939 28
Weapons 81,223  16,622  0.3%    12,203  20
Other Regulated Goods   216,562 141,514 2.3%    103,157 30
Hate Speech 267,373 57,905  0.9%    49,975  32
*Correctly and consistently enforcing against false information is a dynamic process that requires up-to-date context and diligence.  As we strive to continually improve the precision of our agents’ enforcement in this category, we have chosen, since H1 2022, to report figures in the "Content Enforced" and "Unique Accounts Enforced" categories that are estimated based on a rigorous quality-assurance review of a statistically significant portion of false information enforcements. Specifically, we sample a statistically significant portion of false information enforcements across each country and quality-check the enforcement decisions. We then use those quality-checked enforcements to derive enforcement rates with a 95% confidence interval (+/- 5% margin of error), which we use to calculate the false information enforcements reported in the Transparency Report. 
Analysis of Content and Account Violations

Our overall reporting and enforcement rates remained fairly similar to the previous six months, with a few exceptions in key categories. We saw an approximate 3% decrease in total content and account reports and enforcements this cycle.
The categories with the most notable fluctuations were Harassment & Bullying, Spam, Weapons, and False Information. Harassment & Bullying saw a ~56% increase in total reports, and a subsequent ~39% increase in content and unique account enforcements. These increases in enforcements were coupled with a ~46% decrease in turnaround time, highlighting the operational efficiencies our team made in enforcing against this type of violating content. Similarly, we saw a ~65% increase in total reports for Spam, with a ~110% increase in content enforcements and ~80% increase in unique accounts enforced, while our teams also reduced turnaround time by ~80%. Our Weapons category saw a ~13% decrease in total reports, and a ~51% decrease in content enforcements and ~53% reduction in unique accounts enforced. Lastly, our False Information category saw a ~14% increase in total reports, but a ~78% decrease in content enforcements and ~74% decrease in unique accounts enforced. This can be attributed to the continued Quality Assurance (QA) process and resourcing we apply to false information reports, to ensure that our moderation teams are accurately catching and actioning false information on the platform.
Overall, while we saw generally similar figures as in the last period, we believe it is important to continue to improve the tools our community uses to actively and accurately report potential violations as they appear on the platform.
Combating Child Sexual Exploitation & Abuse

The sexual exploitation of any member of our community, especially minors, is illegal, abhorrent, and prohibited by our Community Guidelines. Preventing, detecting, and eradicating Child Sexual Exploitation and Abuse Imagery (CSEAI) on our platform is a top priority for Snap, and we continually evolve our capabilities to combat these and other crimes.
We use active technology detection tools, such as PhotoDNA robust hash-matching and Google’s Child Sexual Abuse Imagery (CSAI) Match to identify known illegal images and videos of child sexual abuse, respectively, and report them to the U.S. National Center for Missing and Exploited Children (NCMEC), as required by law. NCMEC then, in turn, coordinates with domestic or international law enforcement, as required.
In the first half of 2023, we proactively detected and actioned 98 percent of the total child sexual exploitation and abuse violations reported here — a 4% increase from the previous period.
Reason  Total Content Enforced  Total Account Deletions Total Submissions to NCMEC*
CSEAI   548,509 228,897 292,489
**Note that each submission to NCMEC can contain multiple pieces of content. The total individual pieces of media submitted to NCMEC is equal to our total content enforced.'"""
    q_text = 'What is the amount of content enforced for the harassment and bullyin reason?'

    t5_prepared_text = f"""
    [INST] [System]: You are a friendly chatbot assistant that responds in a conversational
    manner to user's questions. Respond in short but complete answers unless specifically
    asked by the user to elaborate on something. Use History to inform your answers.
    Cite the exact source file your information comes from. Create only a single answer.
	If the question is in another language than English, make sure the answer is in the question's language.
    If none of the texts provided in the context contain the answer to the question, do not answer and state that you do not have enough information.
    ---
    [Context]: {data}
    --- [/INST]
    [User]: {q_text}
    ---
    [Answer]:
    """

    tokenized_text = tokenizer(t5_prepared_text,
                                  return_tensors="pt").input_ids.to('cuda')

    summary_ids = model.generate(tokenized_text, min_new_tokens=100, max_new_tokens=512)

    out = tokenizer.batch_decode(summary_ids,
                                 skip_special_tokens= True)[-1]

# print(out)

    out_split = out.split('[Answer]:')
    ans = out_split[-1]
    question = out_split[0].split('[User]:')[-1]

    sources = re.findall(r"(?P<url>https?://[^\s]+)", ans)

    src_str = ""
    for source in sources:
        src_str += f"{source}\n"

    url_regex = "(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)"

    print(f'Question: {question}')
    print(f'Answer: {ans}')

    with open(f'{answer_file}/{q_id:03d}.txt', 'w') as file:
        file.write(ans)
    with open(f'{source_file}/{q_id:03d}.txt', 'w') as file:
        file.write(str(src_str))
