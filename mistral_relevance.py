# SPDX-FileCopyrightText: 2024 piistis
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import torch
import pandas as pd
from transformers import AutoTokenizer, AutoModelForCausalLM, BitsAndBytesConfig, pipeline
import pathlib
import time
import elasticsearch
import os
import re

import PIISTISRetrieval

# Run git clone https://code.peren.fr/hackathon-2024/retrieval-modules/platform-docs-versions.git
# before the first run of this code

repo = pathlib.Path("platform-docs-versions")

index_input = PIISTISRetrieval.transform_mds_to_indexable_dictionaries(repo.rglob("*.md"))
index_input_with_segments = PIISTISRetrieval.add_chunked_text(index_input)

chunks = []

for file in index_input_with_segments:
    for chunk in file['document_text_segmented']:
        chunks.append({'source': file['source'], 'text': chunk})

texts = []
sources = []

# Extract chunks of data from the markdown files

for path in repo.rglob("*/*.md"):
    f = open(path, "r", encoding="utf-8")
    split = f.read().split()
    n = 200 # Set the chunk length (in words)
    for i in range(0, len(split), n):
        texts.append(' '.join(split[i:i+n]))
        sources.append(str(path))

df = pd.DataFrame.from_dict({'text': texts, 'source': sources})
# df.to_csv('./data/preprocessed.csv')

data = []

# Take 20 random chunks

rand_docs = df.sample(20)

for i in range(len(rand_docs)):
    row = rand_docs.iloc[i]
    data.append({'source': row.source, 'text': row.text})

print(len(data))

bnb_config = BitsAndBytesConfig(
    load_in_4bit=True,
    bnb_4bit_quant_type="nf4",
    bnb_4bit_use_double_quant=True,
)

model = AutoModelForCausalLM.from_pretrained('mistralai/Mistral-7B-Instruct-v0.2',
                                            device_map="auto",
                                            load_in_4bit=True,
                                            quantization_config=bnb_config,
                                            torch_dtype=torch.bfloat16)

tokenizer = AutoTokenizer.from_pretrained('mistralai/Mistral-7B-Instruct-v0.2')

questions = pd.read_csv("./challenge-2-dataset-and-documentation/dataset/train/input/questions.csv", sep=';')

curr_time = str(time.time()).split('.')[0]

out_file = f"./output{curr_time}/"
os.mkdir(out_file)

answer_file = f"./output{curr_time}/answers/"
os.mkdir(answer_file)

source_file = f"./output{curr_time}/sources/"
os.mkdir(source_file)

# [INST] and [/INST] are tokens used in mistral training for marking the start and end of an instruction
for i in range(min(50, len(chunks))):
    chunk = chunks[i]
    text = chunk['text']
    source = chunk['source']

    t5_prepared_text = f"""
    [INST]
    [System]: You are a friendly chatbot assistant that determines whether a piece of text contains relevant
    information to a question or answers the question. Respond either just yes or just no.
    Create a single word answer.


    This is the text you will be asked about:

    {text}; source: {source}

    End of text.

    Question: How many pieces of harassment has LinkedIn removed in 2019?
    Say whether the text contains any information that could help answer the question above.
    [/INST]

    Answer:
    """

    tokenized_text = tokenizer(t5_prepared_text,
                                  return_tensors="pt").input_ids.to('cuda')

    summary_ids = model.generate(tokenized_text, min_new_tokens=1, max_new_tokens=10)

    out = tokenizer.batch_decode(summary_ids)[-1]

    out_split = out.split('[Answer]:')
    ans = out_split[-1]

    print()
    print(text)
    print()
    print(ans)

    sources = re.findall(r"(?P<url>https?://[^\s]+)", ans)

    src_str = ""
    for source in sources:
        src_str += f"{source}\n"

    url_regex = "(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)"

    with open(f'{answer_file}/{i:03d}.txt', 'w') as file:
        file.write(ans)
    with open(f'{source_file}/{i:03d}.txt', 'w') as file:
        file.write(str(src_str))
