1

Published October 2023



EU StoreTransparency Report

2



03

06

10

13

16

18

19

22

25



Introduction

Robust proactive controls

Innovative tools

Automated and expert content moderation

Notices and regulatory contacts

Complaint and dispute resolution

Holding bad actors accountable

Collaborating across partners and industry

Conclusion



Table of contents

3



Introduction



Nearly three decades ago, Amazon set out to be Earth’s most customer-centric company, wherepeople can discover and purchase the widest possible selection of safe and authentic goods. As part ofthat mission, we obsess over earning and maintaining trust by ensuring that we provide a trustworthy

shopping experience. We believe that customer trust is difficult to earn and easy to lose. We invest

heavily in people and technology to protect customers, selling partners, brands, and advertisers fromany form of fraud or abuse.



This is the first of the Amazon EU Store bi-annual Transparency Reports that will also cover

requirements as part of the EU Digital Services Act (DSA). This report sets out how Amazon hasinvested in ensuring a trustworthy shopping experience and continues to raise the bar in keeping ourEU store safe for customers, selling partners, brands, and advertisers and includes data from Januarythrough June of 2023.



Globally in 2022, we invested more than $1.2 billion and employed over 15,000people—including machine learning scientists, software developers, and expertinvestigators—dedicated to protecting customers, brands, selling partners, and ourstore from counterfeit, fraud, and other forms of abuse.



Investments in people and technology

4



We are proud of the progress we have made in preventing content that is illegal or violates our terms

and conditions from being available in our store. This has required significant resources, innovation by

Amazon, and partnerships that we have built with rights owners, government agencies, law enforcement,IP organisations, and many others. We have established best practices that can be applied acrossthe retail industry globally—in our proactive controls, our innovative tools, and for how the privateand public sector can work together to provide consumers, small businesses, and selling partners atrustworthy shopping experience. While we believe we have made a great deal of progress, we continueto invest in improving our shopping and selling experience. We also believe that the industry still has along way to go. Amazon continues to be committed to investing, innovating, and being a great partner.

Founded in 1994, Amazon started as a retailer for books. In 2001, Amazon opened its store to third-

party sellers. We opened our first store in the European Union (EU) in 1998, in Germany. Over the

last 25 years, we’ve contributed to the growth of local communities and created jobs and economicopportunities in most of the EU Member States and in all kinds of locations, from isolated rural and

neglected post-industrial areas to city centres and campuses. Today, Amazon operates stores in Germany,

Italy, France, Spain, Netherlands, Sweden, Poland, and Belgium, and we employ people across manyother EU Member States. We directly employ more than 150,000 people in permanent roles across 21EU Member States, including more than 35,000 people in professional functions. We have corporate

offices in approximately 50 European cities, including 11 cities in Germany, five in France, five in Italy, and

two in Spain. We’ve also invested in 15 research and development centres in nine Member States, andwe operate more than 250 logistics centres across the EU. These resources help to service an estimated

181,368,208 average monthly users across the EU.



Average monthly active users



Member state country Average monthly active users

Austria 5,698,882

Belgium 2,781,420

Bulgaria 82,082

Croatia 143,992

Cyprus 76,504

Czech Republic 167,353

Denmark 269,845

Estonia 63,989

Finland 180,653

France 34,617,763

Germany 60,390,505

Greece 171,397

Hungary 116,326

5



Ireland 1,802,267

Italy 38,121,014

Latvia 61,579

Lithuania 69,259

Luxembourg 408,565

Malta 76,491

Netherlands 4,589,643

Poland 2,452,715

Portugal 1,536,009

Romania 140,609

Slovakia 51,728

Slovenia 163,706

Spain 25,101,320

Sweden 2,032,592

6



Robust proactive controls



Our voluntary controls use advanced machine learning techniques and automation to monitor different

aspects of our store for potentially fraudulent, infringing, inauthentic, non-compliant or unsafe products

or content to maintain a trustworthy shopping experience. Our automated detection tools operate

continuously throughout every step of selling in our store, starting from when a prospective seller beginstheir registration process to listing or updating a product, changing key account information, receiving afunds disbursement, and more. In most cases, bad actors are stopped from even creating an account orlisting a single product for sale, and prohibited content is stopped before a customer ever sees it.



Our robust seller verification coupled with our efforts to hold bad actors accountable

are working. The number of bad actor attempts to create new selling accounts

decreased from 6 million attempts in 2020, to 2.5 million attempts in 2021, to800,000 attempts in 2022.

Robust, upfront vetting



Seller verification

Amazon uses advanced technology and expert human reviewers to verify the identities of potentialsellers. Prospective sellers are required to provide a variety of information, such as government-issuedphoto IDs, taxpayer details, and banking information. In addition to verifying these, Amazon’s systemsalso analyse numerous data points including behaviour signals to detect and prevent risks, includingconnections to previously detected bad actors. To ensure authenticity of the individual identity, we also

employ live verification methods, like video-based verification or in-person appointments, ensuring that

it’s straightforward for honest small businesses to start selling while making it challenging for maliciousactors to create new selling accounts..

7



Product safety and compliance

Our content moderation systems aimed at product compliance include controls that function through

automated rules to identify and remove non-compliant products. We employ thousands of keyword-based algorithms and machine learning models that are continuously run against the EU store’s product

catalogue, considering linguistic differences and local compliance requirements by EU storefront location,

to identify potential policy violations. These controls aim to prevent non-compliant products from being

listed or flag them for Amazon’s expert investigators so listings can be stopped if compliance issues are

found or additional information is needed from sellers.



Automated brand protections

Amazon’s Intellectual Property Policy prohibits listings that violate rights owners’ intellectual property

rights. Amazon Brand Registry, a free service launched in 2017, enables brands to more effectively

protect their intellectual property, whether or not they sell on Amazon. Through Brand Registry, brandscan share IP and product data, which Amazon uses to prevent potential infringements. The purpose ofthese automated brand protections is to detect content that likely infringes the intellectual propertyrights of brands and other rights owners. For example, our brand protection tools use advanced machinelearning to scan keywords, text, and logos which are identical or similar to registered trademarks orcopyrighted work, in order to prevent attempted listing of counterfeit or infringing products.



Globally, Amazon’s automated technology scans over 8 billion daily attemptedchanges to product detail pages for signs of potential abuse.



Continuous monitoring

8



Advertising

We proactively detect and remove advertising content that violates our Ad Policies, which are designed tomaintain a high customer experience bar for ads on the store. We require all advertising content to complywith all applicable laws, rules, and regulations; to be appropriate for a general audience, and honest about

the products or services that ad promotes. For example, we prohibit deceptive, misleading or offensiveads, as well as certain sexual, violent or offensive content.



We invest heavily in people and technology to protect customers, brands, advertisers, and the EU storefrom fraud and other forms of abuse. Amazon deploys a number of measures to ensure compliancewith our Ad Policies and detect infringing ads, including through automated moderation tools thatcheck millions of ads and their visible ad elements per day worldwide (including advertiser-suppliedimages, product listing titles and images, and product descriptions). For example, we implement deny

lists on certain products that block all ads for customers who search for specific query terms e.g. “guns”.Ad Policies also block specific listings for being viable for advertising. To complement our automated

measures, expert teams also conduct human reviews of ads to identify any potential non-compliance andapply the learnings as feedback to continually improve our automated moderation tools.



Trustworthy reviews

Our moderation processes for community content include machine learning models that detect contentthat violates our Community Guidelines and prevent it from being published. We strictly prohibit fake

reviews that intentionally mislead customers by providing information that is not impartial, authentic,

or intended for that product or service. We invest significant resources to proactively stop fake reviews.

This includes machine learning models that detect risk, including relationships between accounts, sign-inactivity, review history, and other indications of unusual behaviour, as well as expert investigators that usesophisticated fraud-detection tools to analyse and prevent fake reviews from ever appearing in our store.

Our machine learning models analyse millions of reviews each week using thousands of data points to

detect risk. The review ranking algorithm considers signals from Amazon’s fraud-detection tools relatedto the authenticity of a review. When we strongly suspect that a review is inauthentic, we suppress thereview completely, so it is not displayed in the Amazon EU store.



Offensive and controversial products

Amazon prohibits the sale of products and books that promote, incite, or glorify hatred, violence, racial,sexual, or religious discrimination or promote organisations with such views; contain pornography, glorifyrape or paedophilia or promote the abuse or sexual exploitation of children; or graphically portray violence

or victims of violence, and advocate terrorism; among other material deemed inappropriate or offensive.We leverage machine learning and automation to filter listing submissions that we suspect of potential

policy violation, and then our content moderation teams manually review these suspect listings. We use

machine learning and manual review to filter potentially policy-violating listings.

9



Voluntary content moderation

In the first half of 2023, we took 274MM actions on our own initiative, which include actions taken

through the proactive content moderation tools we have built to remove content from our EU store, aswell as those related to policy violations or other types of non-illegal content.



Number of actions taken on our own initiative by type of restriction



Type of restriction \# of Actions

Remove content 84.2MM

Disable access to content 133.6MM

Suspend monetary payments 313K

Partially suspend provision of the service 51.5MM

Totally suspend provision of the service 385K

Suspend the account 4.2MM

Make another restriction 258K

All others 0



Number of actions taken on our own initiative by type of content



Related to \# of Actions

Product 219.8MM

Multimedia (including Image, Text, and Video) 54.6MM

All others 0

10



Our teams are constantly innovating on behalf of customers, brands and selling partners to create a safe

and trustworthy shopping experience. This includes building tools that we provide to brands and sellingpartners to help them comply with applicable laws and our terms and conditions and also empowerthem to provide us with feedback and information that we use to improve our proactive controls andautomated content moderation.



Business and educational tools for selling partners

We have straightforward policies and a suite of powerful tools available for sellers to ensure their

products are offered in accordance with applicable laws. Entrepreneurs and small businesses can usethese policies as a guide to get started in our store and list their first products after they undergo ourseller verification process.



When we innovate to improve the experience of selling on Amazon, we start by listening to our selling

partners. Our selling partner insights programmes seek feedback on our features and processes by

polling selling partners when they log in to their selling accounts, sharing ad-hoc surveys, and hostinginteractive workshops with our teams. Selling partners can contact us in a variety of ways, including by

email, phone, and chat, and we also analyse selling partner contacts to detect and fix the drivers of these

issues and improve our help content and processes.



Our new tools and services help selling partners launch new products, optimise listings, and expand their

businesses globally. We continue to keep selling partners informed, with tips on how to optimise theirAmazon selling experience, as well as updates on new regulatory requirements and policies, in regularnews announcements via Seller Central, seller forums, newsletters, and our seller app.

Amazon EU store’s Intellectual Property Policy provides clear and practical information to sellers aboutintellectual property rights and common intellectual property concerns that might arise when selling inAmazon’s store, including regarding the enforcement of those rights. Amazon has no tolerance for badactors that are attempting to intentionally abuse or circumvent these policies, but we also recognise thathonest, well-intending sellers share Amazon’s mission to protect consumers while respecting the IP

rights of others, but some may unknowingly list a non-compliant or prohibited product because they areunaware of an applicable legal requirement or Amazon policy.



Innovative tools

11



Our Seller University helps European selling partners learn and master Amazon’s tools and grow theirbusinesses by offering courses on hundreds of topics, including how to start selling on Amazon, howFulfilment by Amazon (FBA) works, and advertising tips for brand owners.



The Amazon EU store also prompts sellers to provide relevant product safety and compliance materials,including product compliance warnings and markings on product pages and high-quality six-sided

images of their products and packaging. Often and where available, we leverage APIs and publicresources to help make compliance easy and reliable. For example, sellers can display energy efficiency

labelling by simply giving us their European Product Registry for Energy Labelling ID information.



Brand protection tools

Amazon creates powerful tools for rights owners to protect their brands by partnering with us. Wework with a large and ever-growing number of brands, and because they know their products best,we work together so we can be even more effective in proactively stopping counterfeit, fraud, andother forms of abuse.

We launched Amazon Brand Registry, a free service for brands, whether they sell in our store or not.The service provides brands the ability to better manage and grow their brand with Amazon, andprotect their brand and intellectual property rights. Through the Report a Violation tool, brand ownerscan more easily search for, identify, and report infringements and subsequently track theirsubmissions within the dedicated Submission History dashboard. They also get access to many otherbrand protection and brand building features.

12



Project Zero combines Amazon’s advanced technology with the sophisticatedknowledge that brands have of their own intellectual property and how best to detectcounterfeits of their brands. This happens through our powerful brand protectiontools, including automated protections, product serialisation capabilities, and theunprecedented ability we give brands in Project Zero to directly remove counterfeitlistings from our store.



Project Zero



For small businesses that are just getting started and are looking for help in obtaining and protectingtheir intellectual property, our IP Accelerator programme connects these businesses with a vetted

network of trusted IP law firms in 39 different countries and 13 different languages, offering high-

quality, trusted trademark registration services at pre-negotiated competitive rates for these smallbusinesses.

We also prevent counterfeits from reaching customers through Transparency, a product serialisationservice that uses codes unique to every individual manufactured unit of a product to identify thoseindividual units. These codes can be scanned throughout the supply chain and by customers to verifyauthenticity using the Amazon Shopping App or Transparency App, regardless of where the products

were purchased. Amazon verifies these codes to ensure that only authentic units are shipped to

customers, virtually eliminating counterfeits for these products.

13



Amazon employs machine learning scientists, data analysts, software developers, and expertinvestigators dedicated to protecting customers, brands, selling partners, and our store from illegalcontent, including counterfeit, fraud, and other forms of abuse. These employees help us drive bothautomated and expert manual content moderation.



Automated and expertcontent moderation



Leveraging automation to drive scaled impact

Our automated tools help us scale our protections and take action more quickly. They help us operate

at scale to prevent bad actors from registering an account, and to detect and remove listings or othercontent that violate our policies or the law. These automated tools range from text-based algorithms

that identify specific keywords to sophisticated image recognition and machine learning models. Onceour tools have identified potentially infringing or illegal content, we use a mixture of automated tools and

expert investigators to determine the appropriate enforcement action.



When our automated tools identify prohibited content with a high degree of confidence, they

automatically take enforcement action. We use the data and learnings gathered from these technologiesand valid notices of infringement or illegal content to innovate and improve our controls.



In the first half of 2023, 73MM of our voluntary actions were fully automated. 97% of our fully automated

voluntary actions were accurate.

14



Safeguards applied to automation

To safeguard against potential errors made by our automated tools, we implement processes to ensure

that we have a high confidence rate that our automated tools operate as intended and to minimise

mistakes. We do this by ensuring our automated tools meet a high bar of accuracy before they arelaunched by testing the provision of the control, and by continuously auditing our automated tools

after they launch and removing from use automation that does not maintain a sufficiently high level

of accuracy. We also constantly improve our automated tools by training them using new information,including internal learnings and developments (including outcomes of expert manual decisions) andexternal risk signals, so they can learn and constantly get better at proactively identifying and blockingnon-compliant products automatically.



Expert manual reviews

All Amazon staff, including staff dedicated to content moderation, are required to meet Amazon’s

Leadership Principles. The Leadership Principles are a set of guidelines that Amazon employees use

every day to solve problems, evaluate trade-offs, and make decisions. There are 16 in total, and they

are the framework of how we evaluate potential candidates for jobs and set the expectations of

performance across Amazon. In addition, the level of qualification and expertise our content moderatorshave is diverse and varies depending on their specific job role. Most of our content moderators have abachelor’s degree in relevant fields of study, including computer science, information technology, datascience, information security, finance, foreign studies, intellectual property, and risk management. Allstaff dedicated to content moderation have demonstrated experience performing research on a variety

of topics, including fraud, abuse, trust, and risk; and have the ability to investigate complex and highlytechnical problems, and perform root cause analysis.

15



Linguistic expertise and training

Although most of our expert investigators are able to make content moderation decisions without

specific linguistic expertise, we do have investigator teams with working proficiency in the nationallanguage of the Amazon EU store that they support, specifically German, French, Spanish, Italian, Dutch,Polish, and Swedish, in addition to English. Those with proficiency in national EU store languages assistwith implementing language-based automated controls and machine-translation technology, defining

local store requirements and policies, auditing corresponding decisions, and interacting with MemberState authorities.



Our expert investigators dedicated to content moderation, including the administration of Amazon’s

notice and action mechanisms, complaints and appeals procedures, are trained to identify illegal content

and content that infringes our terms and conditions. Our investigators receive: an exhaustive onboarding

process to familiarise themselves with the underlying policies and standardised operating procedures,which must be completed before they are able to take their own moderation decisions; robust continuedon-the-job training and periodical knowledge tests, including on any new tools or processes; and whenneeded, support from subject matter coaches and escalation paths to team managers. This includestraining in the relevant subject matter to better protect our customers and, additionally, regular training

on our company’s policies, terms and conditions, and their specific area of expertise, whether that’s

product safety and compliance, IP and brand protection, controversial content, or misleading customerreviews. For example, investigators dedicated to evaluating IP infringement notices receive training

and support in accurately identifying different types of infringing listing content, including trademarks,

copyright, design and patents. Similarly, investigators creating product listing rules have detailedknowledge of Amazon’s catalogue and are trained to accurately develop and apply product listing rules.

16



Through the tools we have built, selling partners, brands, and customers can submit notices that alertAmazon when they think they have found illegal content in our store. When we receive a notice, we takeaction quickly to investigate and if accurate, remove content from our store.



Notices and regulatorycontacts



Article 16 notices

In the first half of 2023, our reporting mechanisms and tools received 417,836 notices. We resolved283,220 notices through automated processes.

We took 810,170 actions on valid notices. Our terms and conditions prohibit any illegal content beingoffered for sale and so all of our actions are taken because the information or content violated both ourpolicies and applicable law. The median time to take action on a notice and confirm our actions with the

submitter was less than one day.



Number of Article 16 notices received by type



Related to \# of Notices

Image 27,980

Product 389,856

All others 0

17



Anti-abuse measures

We take the notices and reports we receive from users seriously. Misuse of our systems can negativelyimpact customers and sellers, and we have measures in place to identify and take action when thesesystems are abused. We investigate and take action against submitters who we suspect or have been

found to submit false reports. In the first half of 2023, we took enforcement action against 1,841 users

of our service for repeatedly submitting unfounded notices and complaints.



Regulatory contacts

If there is an instance where a bad actor or content has evaded our proactive controls or notice systemsand we receive a contact from a regulator, we move quickly to respond and resolve the issue. In the

first half of 2023, we received 1,081 contacts from EU Member States’ authorities. The median time

to inform an authority we received their contact was less than one day, and the median time to resolve

the issue they surfaced was two days. Of those contacts, 1,081 were related to product and we had nocontacts related to App, Audio, Image, Synthetic Media, Text, Video, or Other.



Regulatory contacts received by EU Member State



Member state country \# of Contacts

Austria 9

Belgium 6

France 56

Germany 754

Ireland 82

Italy 24

Luxembourg 18

Netherlands 4

Poland 6

Spain 102

Sweden 20

All others 0

18



When we identify and remove a non-compliant or unsafe product offer or suspend a seller, advertiser,

or rights owner due to a policy violation, we provide clear and actionable communications. We describethe policy violation that led to the enforcement action, and also provide additional information aboutAmazon’s policies and compliance resources that can help users be compliant in the future. We have aprocess to remediate policy violations, appeal enforcements, or dispute enforcements and ask Amazonto re-examine decisions.



Complaint and disputeresolution



Complaint resolution

In the first half of 2023, we received 156,369 complaints. 2,868 complaints were because the user

disagreed with our decision not to take action on a notice of alleged illegal content. 153,501 complaints

were because they disagreed with the specific action we took in response to the user uploaded content.Of the 810,170 actions we took on valid notices we received, we later reversed 41,167 of our originaldecisions due to complaints. Our median time to resolve a complaint was one day.



Out-of-court disputes

If sellers remain dissatisfied with an Amazon decision after reaching out to our support teams, they can

seek resolution for most disputes through an independent mediation process, facilitated by the Centre for

Effective Dispute Resolution. This redress mechanism enhances Amazon’s ability to appropriately protect

sellers’ interests and expression. We also have organised teams dedicated to ensuring that we hear andaddress selling partner pain points.



In the first half of 2023, EU selling partners submitted three out-of-court disputes. In these three

mediations, the independent mediator issued three settlement outcomes favourable to Amazon andthere were no recommendations to implement. The median time for a mediator to complete settlement

procedures was 56 days, which is the time from when the mediator notifies Amazon of the dispute to

when the mediator shares their recommendation with us.

19



In partnership with brands and law enforcement, we have been able to hold more bad actors accountablethrough civil litigation and criminal referrals to law enforcement organisations—working to stop them

from abusing our and other retailers’ stores across the industry in the future. Our efforts to identify and

help dismantle counterfeit organisations, fake reviews brokers, and other fraudsters are still early but are

working. We are proud of our efforts so far and how they have helped ensure that far more criminals are

held accountable, but we also believe that there is far more for industry and government to do in holdingbad actors accountable.

We also know honest users sometimes make mistakes. However, we have no tolerance for intentional and

repeated abuse of our systems and we take necessary action to stop abuse in our store. In the first half of

2023, Amazon took enforcement action against 15,774 users of our service for publishing illegal content.



We also responded to 8,863 legal requests from EU Member States’ authorities for information about

users of our service in the legally mandated time-frames.



Legal requests from EU Member States’ authorities

Holding bad actorsaccountable



Member state country \# of Requests

Belgium 86

France 817

Germany 4,513

Italy 1,101

Luxembourg 9

Netherlands 68

Poland 38

Spain 2,206

Sweden 25

All others 0

20



Disrupting counterfeit networks across the globe

We continue to work with brands and law enforcement to hold more counterfeiters accountable, to deterthese criminals from abusing our store, and to stop them from selling counterfeits anywhere. Amazon’sCounterfeit Crimes Unit (CCU) works with brands, customs agencies, and law enforcement to track downcounterfeiters, shut down bad actors’ accounts, seize counterfeit inventory, and prosecute those involved.CCU has disrupted counterfeiters and their networks through civil suits, joint enforcement actions, and

seizures with law enforcement worldwide. When Amazon identifies an issue, we act quickly to protect

customers, brands, and our store, including removing the problematic content or listing and, whereappropriate, blocking accounts, withholding funds, quarantining physical inventory, or referring bad actorsto law enforcement.

In 2022, Amazon’s CCU sued or referred for criminal investigation over 1,300 counterfeiters globally. We

also work to find the factories and the warehouses where these goods are created or stored, and getthem shut down. In 2022, we identified, seized, and appropriately disposed of over 6 million counterfeits,

preventing them from being resold anywhere in the supply chain.



German law enforcement acted on intelligence from Amazon against ninesuspected members of a German-based counterfeit printer ink and toner ringthat attempted to deceive customers by selling fake toner cartridges that weremarketed as genuine products.



Amazon and Salvatore Ferragamo jointly filed two lawsuits against four individuals

(the “defendants”) and three entities for counterfeiting Ferragamo’s products. The

defendants attempted to offer the infringing products in Amazon’s store, violating

Amazon’s policies, Ferragamo’s intellectual property rights, and the law.



Amazon has already filed multiple litigations against fake review brokers across the

EU Member states. In Germany, for example, this litigation led to the fake reviewbrokers 100 Rabatt and Nice Rebate being shut down.



Holding bad actors accountable in the EU

21



Taking action against fake reviews brokers

Our goal is to ensure that every review in Amazon’s store is trustworthy and reflects customers’ actual

experiences. For that reason, Amazon welcomes authentic reviews—whether positive or negative—butstrictly prohibits fake reviews that intentionally mislead customers by providing information that is notimpartial, authentic, or intended for that product or service. Amazon has been pursuing legal actionsagainst fake reviews brokers to combat the root cause of fake reviews in the retail industry. Amazon haswon dozens of injunctions, particularly in Europe, resulting in several paid-review companies being shutdown and halting their activities.



In 2023, Amazon has already filed multiple litigations across Europe, including in Germany, Spain, Poland,Austria, and France. Our legal actions globally are driving positive results as we have shut down some of

the largest global brokers, including Matronex and Climbazon. By taking such action, Amazon targetsthe source of the problem. Because fake review brokers use third-party services like social media andencrypted third-party messaging services to facilitate their illicit schemes, Amazon investigates and

regularly reports abusive groups, deceptive influencers, and other bad actors to these third parties social

media and message services.

22



Collaborating across partnersand industry



We know that we can be more effective by working together across the private and public sector.

We regularly engage with other interested parties from industry participants, consumer protectionorganisations, governments and regulators, academia, and others that share our desire to workcollaboratively to protect consumers and small businesses. We have launched private sector information-sharing agreements and participated in voluntary product safety pledges with governments all overthe world, and continue to seek out other opportunities to partner more closely with other industrymembers and governments where it can drive positive, substantive impact.



Private sector information-sharing

We believe that there should be more private sector information-sharing. As we laid out in our 2021blueprint for stopping counterfeiters and our 2023 blueprint for stopping fake reviews, we think it’scritical that private and public sector partnership includes greater sharing of information.



Our membership in the Anti-Counterfeiting Exchange, which is an industry collaboration that startedin the US, is designed to make it more difficult for counterfeiters to move among different stores, andsafer for consumers to shop anywhere they choose. We are eager to see the same or similar effortsacross the globe, so we can all use this type of information in our ongoing efforts to detect and addresscounterfeiting, and we look forward to leveraging jurisdiction and region-specific best practices here

in the EU, such as those laid out in the IP Toolbox against Counterfeiting and the framework of the EUMemorandum of Understanding, to enable conversations and drive European industry-wide data-sharing on counterfeiters. Similarly, our recent announcement of a global Coalition for Trusted Reviewsis an industry collaboration across the US, EU, and other countries to, among other things, shareinformation on how fraudsters operate so we can make even greater progress in decreasing fraudulentreviews.

23



Protecting our borders

Amazon also wants to see greater information-sharing to stop counterfeits at the borders. We continueto expand our work with customs agencies to mutually exchange information on counterfeit activity. We

can aid customs agencies in their detection, search and seizure efforts, and strengthen law enforcement’s

ability to dismantle criminal networks behind these illicit goods. Customs agencies can work with usto not only stop the shipments they seize, but to also help freeze other assets and inventory fromcounterfeiters that we may know about.



Partnering with law enforcement

In addition to the efforts mentioned earlier on stopping counterfeiters and fake reviews brokers where

Amazon partners closely with law enforcement, Amazon also shares information on potential suspiciouscustomer transactions and relevant data points with law enforcement agencies across Europe, such ascustomer information in accordance with the Explosive Precursor Regulation. For this purpose, Amazon

invests significant efforts into identifying and reporting transactions that may be suspicious whencombined with information that law enforcement may have. Amazon has classified several hundred

thousand products for which transactions are monitored, and complex combination purchases are

flagged for potentially being suspicious. All of the results are reviewed by risk managers, to ensure

correct reporting considering the impact an incorrect report might have on our customers and on the

general public. Our efforts in working with the authorities and law enforcement agencies is underlinedby the fact that we are actively participating in the EU Standing Committee on Precursors, the German

Arbeitskreis on Explosive Precursors, and are in close contact with the relevant national contact points. In

light of this participation, we have contributed to the Guidance Documents released by the Commissionon the identification and reporting of suspicious precursors.

24



Partnering with law enforcement

Product safety pledges

Amazon has signed four product safety pledges across the world in the EU, Australia, Japan, andCanada. Each pledge commits signatories to meet certain standards like actioning on recalled product

notifications from governments efficiently or providing data to regulatory partners to help inform and

improve their processes and compliance laws. As a founding signatory of the EU Product Safety Pledge

in 2018, Amazon was pleased to continue our cooperation with the European Commission by signingan updated agreement—Product Safety Pledge+ in March 2023, which will go into effect in December.The original 2018 Product Safety Pledge was the first of its kind, demonstrating the value of bringingtogether key stakeholders and taking a pragmatic approach with clear benefits for consumers.



The new Pledge+ features commitments beyond what is established in EU safety legislation,strengthening cooperation and dialogue between signatories and authorities to protect consumers. Inaddition, the pledge has served as the backdrop to a pilot project between consumer groups and thepledge’s signatories, meant to facilitate the exchange of information and timely coordinated action forthe takedown of unsafe products.



Amazon’s Director of EU Public Policy, James Waterworth (far left), with representatives from other companies at the signingceremony for the European Commission’s ‘Product Safety Pledge+’

25



Amazon is known for its customer obsession, and an essential part of that is earning and maintaining our

customers’ and selling partners’ trust. Inherent to that – we do not sacrifice customer safety or long-term

customer trust for short-term gain. It is the reason we invest far above and beyond our legal obligationsto ensure a trustworthy shopping and selling experience.

We recognise that our job of protecting our customers, brands, and selling partners is never done—inthis area, as with the rest of Amazon, we always perceive that it is Day 1 and that we must continue toinnovate and get even better than where we are today.

In addition to our robust and proactive controls, we are continuously creating new tools and advancingour technology to detect bad actors and illegal content and stop it from being found in our store. We

head off fraud, counterfeiting, and inappropriate content before customers ever see it our store. In

cases where we have missed something and it is reported to us, we swiftly remove the content, hold

bad actors accountable, and use such incidents to inform our prevention and monitoring efforts going

forward.

We will continue to innovate and join forces with industry and governments to improve outcomes for

consumers. We will continue to post updates to our ongoing efforts via this report every six months,

including those areas required for reporting by the DSA.



Conclusion

26