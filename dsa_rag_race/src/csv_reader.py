# SPDX-FileCopyrightText: 2024 piistis
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

# TEST

import csv

# Open the CSV file
with open('../data/questions.csv', 'r') as csvfile:

    # Create a CSV reader object
    reader = csv.reader(csvfile, delimiter=';')

    headers = next(reader)

    # Read each row of the CSV file
    for row in reader:
        # print(row)
        index = row[0]
        question = row[1]
        
        print(f"index: {index} --- question: {question}")
        