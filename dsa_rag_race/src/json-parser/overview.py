# SPDX-FileCopyrightText: 2024 piistis
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import json
from tabulate import tabulate

file_name = "schema.json"
# file_name = "TikTok_QueryVideos.json"
# file_name = "Twitter_APIv2-Usage.json"


def extract_fields(json_data):
    fields = []
    properties = json_data.get("properties", {})

    for key, value in properties.items():
        description = value.get("description", "")
        fields.append((key, description))

    return fields


with open("../../data/" + file_name, "r") as file:
    data = json.load(file)

fields = extract_fields(data)

print(tabulate(fields, headers=["Field", "Description"], tablefmt="grid"))


