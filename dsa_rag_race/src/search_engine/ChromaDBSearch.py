# SPDX-FileCopyrightText: 2024 piistis
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import PIISTISRetrieval
import chromadb
import numpy as np
import csv

def index(list_of_md_files, collection_name):

    index_input = PIISTISRetrieval.transform_mds_to_indexable_dictionaries(list_of_md_files)
    index_input_with_segments = PIISTISRetrieval.add_chunked_text(index_input)

    chroma_client = chromadb.Client()
    collection = collection = chroma_client.create_collection(name=collection_name)

    input_documents = []
    input_metadatas = []

    for document in index_input_with_segments:
        for segment in document['document_text_segmented']:
            input_documents.append(segment)
            metadata = {}
            metadata['source'] = document['source']
            metadata['platform'] = document['platform']
            metadata['md_file_name'] = '%s/%s' % (document['platform'], document['md_file_name'])
            metadata['segment'] = segment
            input_metadatas.append(metadata)

    # transform the indexable dictionary to a chromadb-compatible format
    input_ids = input_ids = [str(x) for x in range(len(input_documents))]

    #collection = chroma_client.create_collection(name="platform-docs-versions-chunked")
    collection.add(documents=input_documents, metadatas=input_metadatas, ids=input_ids)

    return collection


def query(collection, query):
    results = collection.query(query_texts = [query])
    return results


def perform_searches_for_csv(input_path, output_path, collection):
    with open(input_path, 'r') as csvfile:
        # Create a CSV reader object
        reader = csv.reader(csvfile, delimiter=';')

        headers = next(reader)

        # Read each row of the CSV file
        for row in reader:
            
            index = row[0]
            question = row[1]

            results = query(collection, question)

            top_n_md = []
            top_n_URL = []
            top_n_segments = []
            
            for result in results['metadatas'][0]:
                top_n_md.append(result['md_file_name'])
                top_n_URL.append(result['source'])
                top_n_segments.append(result['segment'])

            f = open('%s/%s_md.txt' % (output_path, index), 'w')
            for md_file in top_n_md:
                f.write(f"{md_file}\n")
            f.close()

            f = open('%s/%s_source.txt' % (output_path, index), 'w')
            for url in top_n_URL:
                f.write(f"{url}\n")
            f.close()

            for segment_index in np.arange(0, len(top_n_segments)):
                segment = top_n_segments[segment_index]
                f = open('%s/%s_segment_%s.txt' % (output_path, index, segment_index), 'w')
                f.write(segment)
                f.close()