# SPDX-FileCopyrightText: 2024 piistis
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import chromadb
from pathlib import Path
import ChromaDBSearch
import numpy as np
import csv

PATH_TO_MD_VERSIONS = '/gpfswork/rech/olh/commun/platform-docs-versions'

list_of_relevant_mds = Path(PATH_TO_MD_VERSIONS).rglob("*.md")

collection = ChromaDBSearch.index(list_of_relevant_mds, 'platform-docs-versions-chunked')

PATH_TO_INTERMEDIATE_OUTPUT = 'ranked_results'
PATH_TO_INPUT = '/gpfswork/rech/olh/commun/dsa_rag_race/data/questions.csv'

Path(PATH_TO_INTERMEDIATE_OUTPUT).mkdir(parents=True, exist_ok=True)

ChromaDBSearch.perform_searches_for_csv(PATH_TO_INPUT, PATH_TO_INTERMEDIATE_OUTPUT, collection)
