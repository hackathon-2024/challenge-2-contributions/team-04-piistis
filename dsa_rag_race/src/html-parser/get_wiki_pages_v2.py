# SPDX-FileCopyrightText: 2024 piistis
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

# Based on https://stackoverflow.com/questions/65516199/exporting-html-data-in-json-format
# and https://brightdata.com/blog/how-tos/how-to-use-beautiful-soup-for-web-scraping-with-python

import json
import requests
from bs4 import BeautifulSoup
from datetime import datetime

# Use this to print all obtained data in the console after the file has been saved.
print_mode = False

# Each time the script is run, a file is created in the /data directory where the data is written to.
# The name of the file has a date and time stamp on it: DD-MM-YY-HMS.
file_name = str(datetime.now().strftime("%d-%m-%Y-%H%M%S")) + '.json'

# We generate wikipedia URLs from each page indicated in the input file pages.
links = ["https://en.wikipedia.org/wiki/" + line.strip() for line in open('pages', 'r').readlines()]

# Get the HTML data and turn it into a BS object.
html_data = {link: str(BeautifulSoup(requests.get(link).content, 'html5lib')) for link in links}

###
# Here, we could do some filtering in the BS object to get the relevant info only.
##

# Write JSON data to file.
with open('../../data/' + file_name, 'w') as f:
    json.dump(html_data, f, indent=4)
    f.close()

if print_mode:
    with open('../../data/' + file_name, 'r') as f:
        print(json.dumps(json.load(f), indent=4))
        f.close()
