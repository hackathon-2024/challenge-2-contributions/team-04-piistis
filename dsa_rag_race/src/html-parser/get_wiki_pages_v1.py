# SPDX-FileCopyrightText: 2024 piistis
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import wikipedia
import json
from datetime import datetime
import re


#================ CONFIG =================

# To limit the amount of data for e.g. test purposes, summary mode can be used such that a summary of the page is
# stored, instead of the whole Wikipedia page.
summary_mode = False

# Use this to print all obtained data after the file/array has been saved to verify data can be retrieved.
print_mode = True

# Use this if one would want to store the data on disk
write_mode = True

# Use this to indicate if you want to read the data from a file. Below, indicate the file to read from.
read_file = None

#============== END_CONFIG ===============


#=============== FUNCTIONS ===============

# Generator function to potentially split a string at length (not used)
def chunk_string(string, length):
    return (string[0+i:length+i] for i in range(0, len(string), length))


# Function that generates the regex expression to distinguish the ((sub)^n)sections within a Wikipedia page.
# The main sections have `depth=2`. See https://regexr.com/7rc2g for expression explanation.
def section_expr(depth):
    return '(?<!=)={' + str(depth) + '}[^=.]+={' + str(depth) + '}'


# Splits the page data into sections such that each section has its own JSON object.
def split_in_sections(title, page, depth=2):
    # Initiate result and regular expression.
    res = {}
    expr = section_expr(depth)

    # Get all the section names of the given depth (and strip them of the '=' symbol).
    section_names = [sec.replace('=', '').strip() for sec in re.findall(expr, page)]

    # Split the input page into the sections (and strip them of the '\n' symbol).
    sections = [re.sub('(\\n)+', ' ', section.strip()) for section in re.split(expr, page)]

    # For all sections, check if there are subsections, and make a JSON object recursively for each subsection.
    # After, assign the section name to the section content.
    for i in range(len(sections)):
        if len(re.findall(section_expr(depth+1), sections[i])) > 0:
            section_name = title if i == 0 else section_names[i-1]
            place = split_in_sections(section_name, sections[i], depth+1)
            sections[i] = place

        if i == 0:
            res[title] = sections[i]
        else:
            res[section_names[i-1]] = sections[i]
    return res

#============= END_FUNCTIONS =============


# In file 'pages', define per line the name of each Wikipedia page one would want to get data from.
file_r = open('pages', 'r')

# Each time the script is run, a file can be created in the /data directory where the data is written to.
# The name of the file has a date and time stamp on it: DD-MM-YY-HMS.
if write_mode:
    file_name = str(datetime.now().strftime("%d-%m-%Y-%H%M%S")) + '.json'
    file_w = open('../../data/' + file_name, 'w')

# In the pages document, indicate which Wikipedia pages should be stored.
# Additionally, store the data in an array such that it could be used further in the script.
all_data = {}
if not read_file:
    for line in [x.strip() for x in file_r.readlines()]:
        # Use the Wikipedia package to obtain summary/page data.
        # auto_suggest is needed if you know that the page exists for sure, see:
        # https://github.com/goldsmith/Wikipedia/issues/192#issuecomment-579850850
        if summary_mode:
            data = wikipedia.summary(line, auto_suggest=False)
        else:
            data = wikipedia.page(line, auto_suggest=False).content

        # Split the page data into its respective sections.
        all_data[line] = split_in_sections(line, data)

        # Append the data to the write file in JSON object.
        # This is not ideal, as page info is now stored as a single string object. We could change this
        # depending on the use case.
        if write_mode:
            json.dump(all_data[line], file_w)
            file_w.write('\n')

# Closing and cleanup
file_r.close()

if write_mode:
    file_w.close()

# This part can verify whether the stored data can be retrieved correctly.
if print_mode:
    if write_mode:
        with open('../../data/' + file_name) as f:
            for line in f:
                print(json.dumps(json.loads(line), indent=4, ensure_ascii=False))
            f.close()
    else:
        for key in all_data.keys():
            print("================ " + key + ": ================")
            print(all_data[key])