# SPDX-FileCopyrightText: 2024 piistis
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import os
import re
from datetime import datetime

file_name = "Transparency Report.md"

tables = []
with open("data/" + file_name, "r", encoding="utf8") as file:
    new_table = []
    table_name = None
    table_found = False
    for line in file.readlines():
        if line.startswith("Table"):
            table_name = "Table " + re.findall('[\d+\.]+\d', line)[0]
            table_found = True
        if table_found:
            new_table.append(line)
        if re.findall('^\d+.', line) and table_found:
            table_found = False
            tables.append((table_name, new_table))
            new_table = []

write_name = str(datetime.now().strftime("%d-%m-%Y-%H%M%S"))

directory = "../../data/" + file_name + "-tables"

if not os.path.exists(directory):
    os.mkdir(directory)

for table in tables:
    with open(directory + "/" + write_name + "-" + table[0] + ".txt", "w", encoding='utf8') as file:
        file.writelines(table[1])




print(tables)




