### Helper Commands for us to use

Being able to rsync our files to the server:

In ```~/.ssh/config```

```
Host infra2.peren.fr
  User hackathon
  IdentityFile ~/.ssh/<your-key-name>
```

Then you can use the rsync jump to send specific files:

```
rsync -e "ssh -o ProxyJump=hackathon@infra2.peren.fr:1502" <file_or_folder> <your_user>@jean-zay.idris.fr:<folder_to_use>
```

You can get the folder to send to by navigating to your $HOME of $WORK folder and then call ```pwd```

The server has git but maybe with key access, this may be annoying.
