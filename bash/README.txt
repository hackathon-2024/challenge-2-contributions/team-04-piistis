To execute the pipeline, run the following scripts:

prepare_test.sh
preprocess_test.sh
run_test.sh

The output will be in output/