#!/usr/bin/bash

# SPDX-FileCopyrightText: 2024 piistis
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

set -e

ROOT=$(dirname $0)
cd $ROOT

module purge
module load pytorch-gpu/py3/2.1.1

python3 -m venv .venv
.venv/bin/pip install --upgrade pip
.venv/bin/pip install --no-cache-dir -r requirements.txt

source .venv/bin/activate

#python magstral_rag.py
