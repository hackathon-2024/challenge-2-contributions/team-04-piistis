# SPDX-FileCopyrightText: 2024 piistis
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import re
from pathlib import Path

# takes as input a list of text lines (i.e. output of a writelines() call) and the name of the original *.md file name)
# outputs a dictionary with info relevant for indexing (source URL, document body text, and the original *.md file name)
def getIndexInput(documentation_content, md_file_name): 
    pattern = r"# Resource URL: (.*)\n"

    document_entries = []
    
    current_source = []
    current_text = ''
    
    for line in documentation_content:
        
        source_url_found = re.findall(pattern, line)
        
        if len(source_url_found) > 0:    
            if len(current_source) > 0:
                document_entries.append({'source' : current_source, 'document_text': current_text, 'md_file_name': md_file_name})
                current_text = ''
                
            current_source = source_url_found[0]
        else:
            current_text += line
    
    if len(current_source) > 0:
        document_entries.append({'source' : current_source, 'document_text': current_text, 'md_file_name': md_file_name})        

    return document_entries


# segments a longer text into smaller chunks, that do not exceed the word_limit
# output is a list of chunks
def segment(source_text, word_limit):
    segments = []

    cleaned_text = " ".join(source_text.split()).strip()

    # crude space-based splitting
    split_cleaned_text = cleaned_text.split()

    current_segment = []
    current_counter = 0
    current_sentence = []
    
    for word in split_cleaned_text:
        current_counter += 1

        current_sentence.append(word)

        # end of sentence? crude splitting on finding a full stop
        if word[-1] == '.':
            current_segment.append(" ".join(current_sentence))
            current_sentence = []
        
        # word limit reached? if so, write current segment
        if current_counter == word_limit:
            # hopefully we had some sentences in the segment. Then current sentence carries over.
            if len(current_segment) > 0:
                segments.append(" ".join(current_segment))
                current_segment = []
                current_counter = len(current_sentence)
            else:
                # sentence exceeded word limit length. Write the tokens as is and reset counter to 0
                segments.append(" ".join(current_sentence))
                current_sentence = []
                current_counter = 0

    # write the last sentence
    current_segment.append(" ".join(current_sentence))
    segments.append(" ".join(current_segment))

    return segments


# takes as input a list of paths to *.md files
# outputs a list of indexable dictionaries
def transform_mds_to_indexable_dictionaries(list_of_md_file_paths):
    indexable_dictionaries = []

    for md_file in list_of_md_file_paths:
        with open(md_file, encoding='utf-8') as current_file:
            documentation_content = current_file.readlines()
            current_index_input = getIndexInput(documentation_content, md_file.name)
            if len(current_index_input) > 0:
                for entry in current_index_input:
                    entry['platform'] = md_file.parents[0].name
                indexable_dictionaries.extend(current_index_input)

    return indexable_dictionaries


# takes as input a list of indexable dictionaries
# adds to each dictionary a 'document_text_segmented' entry
# with a list of segmented texts according to a word limit
def add_chunked_text(indexable_dictionaries, word_limit=200):

    enriched_dictionaries = []

    for dictionary in indexable_dictionaries:
        current_text = dictionary['document_text']
        current_text_segmented = segment(current_text, word_limit)
        enriched_dictionary = {}
        
        for key in dictionary:
            enriched_dictionary[key] = dictionary[key]
        
        enriched_dictionary['document_text_segmented'] = current_text_segmented
        enriched_dictionaries.append(enriched_dictionary)


    return enriched_dictionaries
